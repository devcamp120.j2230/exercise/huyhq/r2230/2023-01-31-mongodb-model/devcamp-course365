const express = require("express");

const {
    getAllCourse,
    getACourse,
    postACourse,
    putACourse,
    deleteACourse
} = require("../controller/courseController");

const couseRoute = express.Router();

couseRoute.get("/course", getAllCourse);
couseRoute.post("/course", postACourse);
couseRoute.get("/course/:id", getACourse);
couseRoute.put("/course/:id", putACourse);
couseRoute.delete("/course/:id", deleteACourse);

module.exports = {couseRoute};