const { default: mongoose } = require("mongoose");

const courseModel = require("../model/courseModel");

const getAllCourse = (req, res) => {
    courseModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: "Lay thong tin thanh cong!",
                course: data
            });
        };
    });
};

const getACourse = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id khong dung dinh dang."
        });
    }

    courseModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: "Lay thong tin thanh cong!",
                course: data
            });
        }
    })
};

const postACourse = (req, res) => {
    var body = req.body;
    if(!body.courseCode){
        return res.status(400).json({
            message: "Ma mon hoc phai bat buoc!"
        })
    };

    if(!body.courseName){
        return res.status(400).json({
            message: "Ten mon hoc phai bat buoc!"
        })
    };

    if(!body.price){
        return res.status(400).json({
            message: "Hoc phi mon hoc phai bat buoc!"
        })
    };

    if(!Number.isInteger(body.price) || body.price < 0){
        return res.status(400).json({
            message: "Hoc phi mon hoc khong dung dinh dang!"
        })
    };

    if(!Number.isInteger(body.discountPrice) || body.discountPrice < 0){
        return res.status(400).json({
            message: "Hoc phi giam gia khong dung dinh dang!"
        })
    };

    if(!body.duration){
        return res.status(400).json({
            message: "Thoi luong mon hoc phai bat buoc!"
        })
    };

    if(!body.level){
        return res.status(400).json({
            message: "Trinh do mon hoc phai bat buoc!"
        })
    };

    if(!body.coverImage){
        return res.status(400).json({
            message: "Anh mon hoc phai bat buoc!"
        })
    };

    if(!body.teacherName){
        return res.status(400).json({
            message: "Ten giao vien phai bat buoc!"
        })
    };

    if(!body.teacherPhoto){
        return res.status(400).json({
            message: "Anh giao vien phai bat buoc!"
        })
    };

    var newCourse = new courseModel({
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending,
    })

    courseModel.create(newCourse, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            });
        } else {
            return res.status(201).json({
                message: "Tao moi khoa hoc thanh cong!",
                course: data
            });
        }
    })
};

const putACourse = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id khong dung dinh dang."
        });
    }

    var body = req.body;
    if(!body.courseCode){
        return res.status(400).json({
            message: "Ma mon hoc phai bat buoc!"
        })
    };

    if(!body.courseName){
        return res.status(400).json({
            message: "Ten mon hoc phai bat buoc!"
        })
    };

    if(!body.price){
        return res.status(400).json({
            message: "Hoc phi mon hoc phai bat buoc!"
        })
    };

    if(!Number.isInteger(body.price) || body.price < 0){
        return res.status(400).json({
            message: "Hoc phi mon hoc khong dung dinh dang!"
        })
    };

    if(!body.duration){
        return res.status(400).json({
            message: "Thoi luong mon hoc phai bat buoc!"
        })
    };

    if(!body.level){
        return res.status(400).json({
            message: "Trinh do mon hoc phai bat buoc!"
        })
    };

    if(!body.coverImage){
        return res.status(400).json({
            message: "Anh mon hoc phai bat buoc!"
        })
    };

    if(!body.teacherName){
        return res.status(400).json({
            message: "Ten giao vien phai bat buoc!"
        })
    };

    if(!body.teacherPhoto){
        return res.status(400).json({
            message: "Anh giao vien phai bat buoc!"
        })
    };

    var course = new courseModel({
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending,
    })

    courseModel.findByIdAndUpdate(id, course, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            });
        } else {
            return res.status(201).json({
                message: `Sua thanh cong id: ${id}`,
                course: data
            });
        }
    })
};

const deleteACourse = (req, res)=>{
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id khong dung dinh dang."
        });
    };

    courseModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            });
        } else {
            return res.status(204).json({
                message: `Xoa thanh cong id: ${id}`,
                course: data
            });
        }
    })
}
module.exports = {
    getAllCourse,
    getACourse,
    postACourse,
    putACourse,
    deleteACourse
};