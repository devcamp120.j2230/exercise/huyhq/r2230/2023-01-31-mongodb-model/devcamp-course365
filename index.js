const express = require("express");                                          //thư viện express

const mongoose = require("mongoose");

const courseModel = require("./app/model/courseModel");

const path = require("path");                                                //thư viện path

const app = express();

app.use(express.json());

const port = 8000;

//connect mongoDb
const nameDb = "CRUD_Course365"
mongoose.connect("mongodb://127.0.0.1:27017/"+nameDb, function (error) {
    if (error) throw error;
    console.log('Successfully connected to DB: '+nameDb);
});
//get route
const {couseRoute} = require("./app/routes/courseRoute");

app.use(express.static(__dirname + "/view"));                               //middleware cho phép load ảnh trong view

app.use("/", couseRoute);

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/view/course365.html"))            //đường dẫn tới file
})

app.listen(port, () => {
    console.log("App on Port: " + port);
})